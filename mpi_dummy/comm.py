# -*- coding: utf-8 -*-

from __future__ import absolute_import
from builtins import object
import inspect

import copy
import numpy as np

from .op import SUM

__all__ = ['Comm', 'Intracomm', 'COMM_WORLD', 'COMM_SELF']

SINGLE_THREADED_Q = None

def decorate_all_methods(decorator):
  def decorate_the_class(cls):
    for name, m in inspect.getmembers(cls, inspect.ismethod):
      if name != '__init__':
        setattr(cls, name, decorator(m))
    return cls
  return decorate_the_class


class Comm(object):
    pass


def running_single_threadedQ():
    global SINGLE_THREADED_Q
    if SINGLE_THREADED_Q is None:
        try:
            from mpi4py import MPI
        except ImportError:
            SINGLE_THREADED_Q = True
        else:
            if MPI.COMM_WORLD.size != 1:
                SINGLE_THREADED_Q = False
            else:
                SINGLE_THREADED_Q = True
    return SINGLE_THREADED_Q


def check_running_single_threaded_decorator(func):
    def inner(*args, **kwargs):
        if not running_single_threadedQ():
            raise RuntimeError("ERROR: MPI_dummy module is running in a " +
                               "mpirun with n>1.")
        else:
            return func(*args, **kwargs)
    return inner


@decorate_all_methods(check_running_single_threaded_decorator)
class Intracomm(Comm):
    def __init__(self, name):
        self.name = name
        self.rank = 0
        self.size = 1

    def Get_rank(self):
        return self.rank

    def Get_size(self):
        return self.size

    def _scattergather_helper(self, sendbuf, recvbuf=None, **kwargs):
        sendbuf = self._unwrapper(sendbuf)
        recvbuf = self._unwrapper(recvbuf)
        if recvbuf is not None:
            recvbuf[:] = sendbuf
            return recvbuf
        else:
            recvbuf = np.copy(sendbuf)
            return recvbuf

    def bcast(self, sendbuf, *args, **kwargs):
        return sendbuf

    def Bcast(self, sendbuf, *args, **kwargs):
        return sendbuf

    def scatter(self, sendbuf, *args, **kwargs):
        return sendbuf[0]

    def Scatter(self, *args, **kwargs):
        return self._scattergather_helper(*args, **kwargs)

    def Scatterv(self, *args, **kwargs):
        return self._scattergather_helper(*args, **kwargs)

    def gather(self, sendbuf, *args, **kwargs):
        return [sendbuf]

    def Gather(self, *args, **kwargs):
        return self._scattergather_helper(*args, **kwargs)

    def Gatherv(self, *args, **kwargs):
        return self._scattergather_helper(*args, **kwargs)

    def allgather(self, sendbuf, *args, **kwargs):
        return [sendbuf]

    def Allgather(self, *args, **kwargs):
        return self._scattergather_helper(*args, **kwargs)

    def Allgatherv(self, *args, **kwargs):
        return self._scattergather_helper(*args, **kwargs)

    def Allreduce(self, sendbuf, recvbuf, op, **kwargs):
        sendbuf = self._unwrapper(sendbuf)
        recvbuf = self._unwrapper(recvbuf)
        recvbuf[:] = sendbuf
        return recvbuf

    def allreduce(self, sendobj, op=SUM, **kwargs):
        if np.isscalar(sendobj):
            return sendobj
        return copy.copy(sendobj)

    def sendrecv(self, sendobj, **kwargs):
        return sendobj

    def _unwrapper(self, x):
        if isinstance(x, list):
            return x[0]
        else:
            return x

    def Barrier(self):
        pass


COMM_WORLD = Intracomm('MPI_dummy_COMM_WORLD')
COMM_SELF = Intracomm('MPI_dummy_COMM_SELF')

# -*- coding: utf-8 -*-
from builtins import str
from builtins import object

__all__ = ['BYTE', 'SHORT', 'UNSIGNED_SHORT', 'UNSIGNED_INT', 'INT', 'LONG',
           'UNSIGNED_LONG', 'LONG_LONG', 'UNSIGNED_LONG_LONG', 'FLOAT',
           'DOUBLE', 'LONG_DOUBLE', 'COMPLEX', 'DOUBLE_COMPLEX']


class _datatype(object):
    def __init__(self, name):
        self.name = str(name)

BYTE = _datatype('MPI_BYTE')
SHORT = _datatype('MPI_SHORT')
UNSIGNED_SHORT = _datatype("MPI_UNSIGNED_SHORT")
UNSIGNED_INT = _datatype("MPI_UNSIGNED_INT")
INT = _datatype("MPI_INT")
LONG = _datatype("MPI_LONG")
UNSIGNED_LONG = _datatype("MPI_UNSIGNED_LONG")
LONG_LONG = _datatype("MPI_LONG_LONG")
UNSIGNED_LONG_LONG = _datatype("MPI_UNSIGNED_LONG_LONG")
FLOAT = _datatype("MPI_FLOAT")
DOUBLE = _datatype("MPI_DOUBLE")
LONG_DOUBLE = _datatype("MPI_LONG_DOUBLE")
COMPLEX = _datatype("MPI_COMPLEX")
DOUBLE_COMPLEX = _datatype("MPI_DOUBLE_COMPLEX")
